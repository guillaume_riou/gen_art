import bpy
import math
from mathutils import Vector
import os
import sys


# import utils (oui c'est dégueulasse)
dir_path = os.path.dirname("/home/guillaume/code/blender_creative_coding/utils/")
if not dir_path in sys.path:
    sys.path.append(dir_path)
    print(sys.path)
from utils import clean_scene, GreasePencil

clean_scene()

gpen = GreasePencil()

s1 = gpen.draw_curve([(0,0,0),(1,3,0), (1,3,0)])
s1.line_width = 30

s2 = gpen.draw_curve([(0,0,0),(2,4,0), (1,3,0)], [(0.3,1)])
s2.line_width = 60

s3 = gpen.draw_curve([(0,0,0),(4,5,0), (1,3,0)], [(1,0.3)])
s3.line_width = 90

gpen.commit()
