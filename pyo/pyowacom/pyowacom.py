from pyo import *
from subprocess import Popen, PIPE
from threading import Thread
l = []
class PyoWacom:
    def __init__(self):
        "docstring"
        # max values of various values for the tablet
        self.max_tilt = 63.33
        self.min_tilt = -64.33
        self.coord_max = [157.48,98.42]
        self.wheel_max = 355.0

    def start(self):
        self.coord = SigTo([0,0])
        self.tilt = SigTo([0,0])
        self.pressure = SigTo(0)
        self.wheelpos = SigTo(0)
        self.buttons = Sig([0 for i in range(5)])
        self.thread = Thread(target=lambda:self._update_sigs())
        self.thread.start()

    def _update_sigs(self):
        libin = Popen(["unbuffer", "libinput", "debug-events"], stdout=PIPE)
        while True:
            line = libin.stdout.readline().decode("utf-8")
            tab_splitted = line.split("\t")
            if "TABLET_TOOL" in line:
                self.tilt.setValue([(float(num.strip()) - self.min_tilt)/(self.max_tilt-self.min_tilt) for num in tab_splitted[3].split("tilt:")[1].replace("*","").split("/")])
                penx, peny = [float(num.strip())/self.coord_max[i] for i, num in enumerate(tab_splitted[2].replace("*","").split("/"))]
                if "pressure" in line:
                    self.pressure.setValue(float(tab_splitted[4].split("pressure:")[1].split(" ")[1].replace("*","")))
            if "TABLET_PAD_RING" in line:
                wheelpos = float(tab_splitted[1].split(" ")[3])/self.wheel_max
                if wheelpos >= 0:
                    self.wheelpos.setValue(wheelpos)
            if "TABLET_PAD_BUTTON" in line:
                button_num = tab_splitted[1].strip().split(" ")[0]
                button_status = 1 if tab_splitted[1].strip().split(" ")[1] == "pressed" else 0

                button_status = 1 if tab_splitted[1].strip().split(" ")[1] == "pressed" else 0
