from pyo import *

class _OSCNode:
    def __init__(self, idle_timer, ramp=0.01, sig_size=3, idle_ramp=1, address="/"):
        self._child_nodes = {}
        self.address = address
        self.sig = SigTo([0]*sig_size, time=ramp)
        self.sig_size = sig_size
        self.idle = True
        self.idle_timer = idle_timer
        self.ramp = ramp
        def set_to_zero():
            if self.idle:
                self.setValue([-400]*sig_size, reset_idle=False, ramp=idle_ramp)
            elif not self.idle:
                self.idle = True
        if idle_timer:
            self.idle_pattern = Pattern(set_to_zero, idle_timer)
            self.idle_pattern.play()

    def setValue(self, vals, reset_idle=True, ramp=False):
        if ramp:
            self.sig.time = ramp
        else:
            self.sig.time = self.ramp
        self.sig.value = vals
        if reset_idle:
            self.idle = False

    def _get_eff(self, key):
        if key not in self._child_nodes:
            self._child_nodes[key] = _OSCNode(self.idle_timer, self.ramp, address=key, sig_size=self.sig_size)
        return self._child_nodes[key]

    def __getitem__(self, key):
        key = str(key)
        return self._get_eff(key)

class OSCToSig:
    """Converts any osc messages sent to that port and osc address to a tree of pyo sigs"""
    def __init__(self, port=13001, idle_timer=False, ramp=0.01, osc_address_pattern="/*", sig_size=1):
        self._get_eff = self.__getitem__
        self._root_node = _OSCNode(idle_timer, ramp, sig_size)
        # values of y between these ranges will go from 0 to 1
        def receive_msg(address, *args):
            addresses = address.split("/")[1:]
            current_node = self._root_node
            for a in addresses:
                # navigate to the targeted node
                current_node = current_node._get_eff(a)
            # set the values of all the sigs
            current_node.setValue(list(args))

        self.osc_rx = OscDataReceive(port, osc_address_pattern, receive_msg)
    def to_dict(self):
        """returns a dict representation of the current values of everything
        this will not work properly with osc streams that assigns values to non leaf addresses.
        """
        val_obj = {}
        def build_val_obj(current_node, obj_ptr):
            if len(current_node._child_nodes.keys()) == 0:
                obj_ptr[current_node.address] = current_node.sig.value
            else:
                obj_ptr[current_node.address] = {}
                a = {}
                for ck in current_node._child_nodes:
                    build_val_obj(current_node._child_nodes[ck], obj_ptr[current_node.address])
        build_val_obj(self._root_node, val_obj)
        return val_obj

    def __getitem__(self, key):
        key = str(key)
        return self._root_node[key]
