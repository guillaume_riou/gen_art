from icecream import ic
events = open("libeventdump").readlines()
max_tilt = 63.33
min_tilt = -64.33
coord_max = [157.48,98.42]
wheel_max = 355.0
for line in events:
    tab_splitted = line.split("\t")
    if "TABLET_TOOL" in line:
        tiltx,tilty = [(float(num.strip()) - min_tilt)/(max_tilt-min_tilt) for num in tab_splitted[3].split("tilt:")[1].replace("*","").split("/")]
        ic([tiltx,tilty])
        penx, peny = [float(num.strip())/coord_max[i] for i, num in enumerate(tab_splitted[2].replace("*","").split("/"))]
        ic([penx,peny])
        if "pressure" in line:
            pressure = float(tab_splitted[4].split("pressure:")[1].split(" ")[1].replace("*",""))
            ic(pressure)
    if "TABLET_PAD_RING" in line:
        wheelpos = float(tab_splitted[1].split(" ")[3])/wheel_max
        if wheelpos >= 0:
            ic(wheelpos)
    if "TABLET_PAD_BUTTON" in line:
        button_num = tab_splitted[1].strip().split(" ")[0]
        button_status = 1 if tab_splitted[1].strip().split(" ")[1] == "pressed" else 0
        ic(button_num,button_status)
