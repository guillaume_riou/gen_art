import pyo
import string
import time

PERMISSIONS = 0
LINKS = 1
OWNER = 2
GROUP = 3
SIZE = 4
MONTH = 5
DAY = 6
HOUR = 7
NAME = 8

class FileSynth:
    def __init__(self, ls_arr, total_size, idx):
        self.envelope = pyo.CosTable([(0,0), (100,1), (500,.3), (8191,0)])
        ls_arr[HOUR] = ls_arr[HOUR].replace(":", "")
        self.beat = pyo.Beat(time=.125, taps=int(ls_arr[HOUR][-1]) + 13, w1=int(ls_arr[DAY]), w2=int(ls_arr[HOUR][0:2])+12, w3=int(ls_arr[HOUR][2:]) + 10, poly=1).play()
        self.tr2 = pyo.TrigEnv(self.beat, table=self.envelope, dur=self.beat['dur'], mul=self.beat['amp'])
        freq = abs(sum([string.ascii_lowercase.find(l) for l in ls_arr[NAME]]) * 10)
        mul = int(ls_arr[SIZE])/total_size
        self.last_audio_object = pyo.Pan(pyo.Sine(freq=freq, mul=self.tr2 * mul), pan=idx)

class FolderSynth:
    def __init__(self, ls_arr, total_size, idx):
        ls_arr[HOUR] = ls_arr[HOUR].replace(":", "")
        self.lfo = pyo.Sine(freq=float(f"0.{ls_arr[LINKS]}")).range(0, int(ls_arr[DAY][-1])/10)
        freq = abs(sum([string.ascii_lowercase.find(l) for l in ls_arr[NAME]]) * 10)
        mul = int(ls_arr[SIZE])/total_size
        if ls_arr[NAME] in ["../", ".."]:
            mul /= 3
        self.a = pyo.ButLP(pyo.SuperSaw(freq, detune=self.lfo, mul=mul), freq=int(ls_arr[HOUR])*2)
        self.last_audio_object = pyo.Pan(self.a, pan=idx)


s = pyo.Server(audio="jack").boot()

current_synth_classes = []
last_synth_classes = []

current_synths = None
current_synths_amp = pyo.SigTo(1, init=1, time=0.5)
current_synths_freq = pyo.SigTo(20000, init=20000, time=0.5)

last_synths = None
last_synths_amp = pyo.SigTo(1, time=0.5)
last_synths_freq = pyo.SigTo(20000, init=20000, time=0.5)

@events.on_chdir
def generate_synths(*args, **kwargs):
    global current_synths
    global current_synths_amp
    global current_synths_freq
    global last_synths
    global last_synths_amp
    global last_synths_freq
    global current_synth_classes
    global last_synth_classes

    ls_strs = $(ls -lap).split("\n")
    ls_arrs = list(filter(None,[list(filter(None, ls_str.split(" "))) for ls_str in ls_strs][1::]))
    ls_file_arrs = list(filter(lambda x: x[NAME][-1] != "/", ls_arrs))
    ls_folder_arrs = list(filter(lambda x: x[NAME][-1] == "/", ls_arrs))
    total_size = sum(int(ls_arr[SIZE]) for ls_arr in ls_arrs)

    last_synth_classes = current_synth_classes
    last_synths = current_synths
    last_synths_amp = current_synths_amp
    last_synths_freq = current_synths_freq

    if len(ls_file_arrs) != 0 or len(ls_folder_arrs) >2:
        current_synth_classes = [*[FileSynth(ls_arr, total_size, i/len(ls_file_arrs)) for i, ls_arr in enumerate(ls_file_arrs)],
                                    *[FolderSynth(ls_arr, total_size, i/len(ls_folder_arrs)) for i, ls_arr in enumerate(ls_folder_arrs)]]
        current_synths_amp = pyo.SigTo(1, init=0, time=0.5)
        current_synths_freq = pyo.SigTo(20000, init=20000, time=0.5)
        current_synths = pyo.ButLP(pyo.Sig([csc.last_audio_object for csc in current_synth_classes]).mix(2),
                                   freq=current_synths_freq,
                                   mul=current_synths_amp).out()
    else:
        current_synths_amp = pyo.SigTo(0.3, init=0, time=0.5)
        current_synths_freq = pyo.SigTo(1000, init=20000, time=0.5)
        current_synths = pyo.ButLP(last_synths.input, freq=current_synths_freq, mul=current_synths_amp).out()

    last_synths_amp.setValue(0)
    last_synths_freq.setValue(0)

generate_synths()
s.start()
