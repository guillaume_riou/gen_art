from pyo import *
from collections import deque
import os
import shutil
import json



class SignalRecord:
    """Records a stream of OSC event as json files"""
    def __init__(self, foldername, framerate=24):
        o = Adsr(attack=0.01, decay=0.32, sustain=0.00, release=0.00, dur=0.3333333)
        self.o = o
        self.om = Metro(0.333333)
        # self.otf = TrigFunc(Metro(0.333333), lambda: o.play())
        self.otf = TrigFunc(o["trig"], lambda: o.play())
        try:
            os.mkdir(foldername)
        except:
            shutil.rmtree(foldername)
            os.mkdir(foldername)
        def dump_frame():
            with open("{}/frame{}.json".format(foldername, self.framenum),"w") as f:
                json.dump({"/":{"pulse":self.o.get()}}, f)
            self.framenum += 1

        self.framenum = 0
        self.pattern = Pattern(dump_frame, 1/framerate)

    def start(self):
        self.pattern.play()
        # self.om.play()
        self.o.play()

s = Server().boot()
a = SignalRecord("pulsesSignal")
a.start()
s.start()
s.gui(locals)
