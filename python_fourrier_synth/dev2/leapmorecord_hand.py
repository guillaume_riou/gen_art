exit_fun = exit
from p5 import *
from PIL import Image, ImageEnhance
import numpy as np
from random import randint
from pyo_osc_tools import *
from pyo import Server


remmax = lambda x: x/x.max()
remmin = lambda x: x - np.amin(x, axis=(0,1), keepdims=True)
touint8 = lambda x: (remmax(remmin(x))*(256-1e-4)).astype(int)

w = 128
h = 128
# w = 256
# h = 256
# w = 512
# h = 512
# w = 980
# h = 720
# w = 1280
# h = 720


class Pulse:
    def __init__(self, pos, speed, grow, rotate, color, max_size, init_size=2):
        self.pos = pos
        self.speed = speed
        self.grow = grow
        self.rotate = rotate
        self.color = color
        self.size = init_size
        self.max_size = max_size
    def move(self):
        self.pos += self.speed
        self.speed.rotate(radians(self.rotate))
        self.size += self.grow
        if self.size > self.max_size or self.size <= 1:
            self.grow = - self.grow

    def draw(self):
        no_stroke()
        fill(*self.color)
        circle(self.pos, self.size)
        alpha = self.size % 1
        fill(*self.color, alpha*255)
        circle(self.pos, self.size + 1)
        self.move()


class SigPulse(Pulse):
    def __init__(self, pos, speed, grow, rotate, color, max_size, dampen_factor, init_size=2):
        super().__init__(pos, speed, grow, rotate, color, max_size, init_size)
        self.dampen_factor = dampen_factor

    def move(self):
        curr_dampen = self.dampen_factor.get()
        self.pos += self.speed * curr_dampen
        self.speed.rotate(radians(self.rotate) * curr_dampen)
        self.size += self.grow * curr_dampen
        if self.size > self.max_size or self.size <= 1:
            self.grow = - self.grow

    def draw(self):
        no_stroke()
        fill(*self.color)
        circle(self.pos, self.size)
        alpha = self.size % 1
        fill(*self.color, alpha*255)
        circle(self.pos, self.size + 1)
        self.move()


class Circle:
    def __init__(self, pos, size, color):
        self.pos = pos
        self.color = color
        self.size = size

    def draw(self):
        no_stroke()
        s = self.size.get()
        alpha = s % 1
        c = [self.color[i].get() for i in range(3)]
        fill(*c, (alpha*255)/5)
        circle((self.pos[0].get(), self.pos[1].get()), s+1)
        fill(*c, 0.2)
        circle((self.pos[0].get(), self.pos[1].get()), s)

folder = input("recording name ? ")

s = Server(buffersize=1024).boot()
# folder = "recordvistest"
recorder = OSCRecord(folder)

p = SfPlayer("./30sec.aiff").out()
recorder.start()
o = recorder.o
nextframe = Trig()
# folder = "recording_example"
# o = OSCRecordSigReader(folder, trigsource=nextframe, loop_playback=False)

def circles_renderer(osc_tree_like):
    o = osc_tree_like
    h1fs = o["hand"][1]["finger"]

    x = [SigTo(Scale(h1fs[i]["pos"].sig[0], -400.4, 1144.4, -1, 1), time=1/24) for i in range(5)]
    y = [SigTo(Scale(h1fs[i]["pos"].sig[2], -178, 473, -1, 1), time=1/24) for i in range(5)]
    z = [SigTo(Scale(h1fs[i]["pos"].sig[1], 0, 473, 1, 0), time=1/24) for i in range(5)]
    velx = [Scale((h1fs[i]["velocity"].sig[0] -400), -2400, 2400, -1, 1) for i in range(5)]
    vely = [Scale((h1fs[i]["velocity"].sig[2] -400), -2400, 2400, -1, 1) for i in range(5)]
    velz = [Scale((h1fs[i]["velocity"].sig[1] -400), -2400, 2400, -1, 1) for i in range(5)]

    # x = [SigTo(Scale(h1fs[i]["velocity"].sig[0], -400.4, 1144.4, -1, 1), time=1/24) for i in range(5)]
    # y = [SigTo(Scale(h1fs[i]["velocity"].sig[2], -178, 473, -1, 1), time=1/24) for i in range(5)]
    # z = [SigTo(Scale(h1fs[i]["velocity"].sig[1], 0, 473, 1, 0), time=1/24) for i in range(5)]

    ditherstr = sum(z)*0.1/w*h
    rpulss = [SigPulse(Vector(0 + randint(1,2) * i, h/2 - randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*2, dampen_factor=3*x[i]) for i in range(1,4)]
    # rpulss.extend([Pulse(Vector(w/2 - randint(1,2) * i, h/2 - randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*2) for i in range(1,4)])
    # ipuls = Pulse(Vector(w/2 - 40, h/2 + 60), Vector(0.8,.06), 3, 3.2, (123,12,223), 54)
    ipulss = [SigPulse(Vector(w/2 + randint(1,2) * i + 8, h/2 + randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/3, (122,12 + i * 8,223 - i *10), i*2, dampen_factor=3*y[i]) for i in range(1,4)]
    ipulss.extend([SigPulse(Vector(w/2 + randint(1,2) * i + 8, 0 + randint(1,2) * 1+i), Vector(0.3*i,0.4*i), 0.2, 1 + randint(1,2) * i/3, (122,12 + i * 8,223 - i *10), i*2, dampen_factor=3*z[i]) for i in range(1,2)])
    def draw_real():
        for rpuls in rpulss:
            rpuls.draw()

    def draw_img():
        for ipuls in ipulss:
            ipuls.draw()

    def draw_frame():
        size(w, h)
        background(0, 0, 0)
        draw_real()
        p5.renderer.flush_geometry()
        real_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
        background(0, 0, 0)
        draw_img()
        p5.renderer.flush_geometry()
        img_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
        pixels = np.array(real_pixels, np.complex)
        pixels.imag = img_pixels

        dither = np.array(np.random.rand(*pixels.shape) * ditherstr.get(), np.complex)
        dither.imag = np.random.rand(*pixels.shape) * ditherstr.get()

        pixels += dither


        views = [np.array(np.fft.fftshift(pixels[:,:,i])) for i in range(3)]
        ffts = [
            np.fft.fftshift(touint8(np.fft.ifft2(views[i])))
            for i in range(3)
        ]
        i = np.array(np.stack(ffts,2), dtype=np.uint8)
        img = Image.fromarray(i, "RGB")
        contrast = ImageEnhance.Contrast(img.convert("RGB"))
        cimg = contrast.enhance(1.2)

        pimg = PImage(w, h, 'RGB')
        pimg._img = contrast.enhance(10)
        pimg._load()
        p5.renderer.tint_enabled = False
        image(pimg, (0,0))
        nextframe.play()
        # save_frame("circles_renderer/test.png")
    return draw_frame


def handspots_renderer(osc_tree_like):
    # sets up the closure
    o = osc_tree_like

    last_shape = ()
    dither = None


    h1fs = o["hand"][1]["finger"]

    randfact = 0
    # rpulss = [Pulse(Vector(0 + randint(1,2) * i, h/2 - randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*2) for i in range(1,4)]
    # x = [Randi(-randfact,randfact) + SigTo(Scale(h1fs[i]["pos"].sig[0], -400.4, 1144.4, -1, 1), time=1/24) for i in range(5)]
    # y = [Randi(-randfact,randfact) + SigTo(Scale(h1fs[i]["pos"].sig[2], -178, 473, -1, 1), time=1/24) for i in range(5)]
    # z = [Randi(0,randfact) + SigTo(Scale(h1fs[i]["pos"].sig[1], 0, 473, 1, 0), time=1/24) for i in range(5)]

    x = [SigTo(Scale(h1fs[i]["pos"].sig[0], -400.4, 1144.4, -1, 1), time=1/24) for i in range(5)]
    y = [SigTo(Scale(h1fs[i]["pos"].sig[2], -178, 473, -1, 1), time=1/24) for i in range(5)]
    z = [SigTo(Scale(h1fs[i]["pos"].sig[1], 0, 473, 1, 0), time=1/24) for i in range(5)]
    ditherstr = z[0]*0.01/w*h

    irrshift = 1
    finger_diff_shift = 0.1
    move_factor = 6
    rpulss = []
    # rpulss.extend([Pulse(Vector(w/2 - randint(1,2) * i, h/2 - randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*2) for i in range(1,4)])
    rpulss.extend( [
        Circle(
            pos=[(x[i] * move_factor + w/2) + (finger_diff_shift * x[i] * (i-2)), (y[i] * move_factor + h/2) + (finger_diff_shift * y[i] * (i-2 ))],
            size=(z[i] * 5),
            color=(x[i] * 2 + move_factor * i ,z[i] * 2 + 45 * i,z[i] * 2 + 27 * i)
            # color=(Sig(204) + 45,Sig(12), Sig(12))
        )
        for i in range(5)
    ])

    # ipuls = Pulse(Vector(w/2 - 40, h/2 + 60), Vector(0.8,.06), 3, 3.2, (123,12,223), 54)
    ipulss = []
    ipulss.extend(
        [
            Circle(
                pos=[(x[i] * move_factor + w/2) + (finger_diff_shift * x[i] * (i-2)), (y[i] * move_factor + h/2) + (finger_diff_shift * y[i] * (i-2 ))],
                size=(z[i] * 7),
                color=(z[i] * 2 + move_factor * i ,z[i] * 2 + 12 * i,z[i] * 2 + 27 * i)
                # color=(Sig(34),Sig(12), Sig(12))
        )
        for i in range(5)
        ]
    )
    # ipulss.extend([Pulse(Vector(w/2 + randint(1,2) * i, h/2 + randint(1,2) * 1+i), Vector(0.0/4*i,0.0/4*i), 0.002, 1 + randint(1,2) * i/3, (12,12 + i,22 - i), i*2) for i in range(1,4)])
    ipulss.extend([Pulse(Vector(w/2 + randint(1,2) * i, 6 + randint(1,2) * 1+i), Vector(0.1*i,0.1*i), 0.002, 1 + randint(1,2) * i/3, (1,1 + i*3,3 - i ), i*2, init_size=14) for i in range(1,2)])
    def draw_real():
        for rpuls in rpulss:
            rpuls.draw()

    def draw_img():
        for ipuls in ipulss:
            ipuls.draw()

    def draw_frame():
        global dither
        size(w, h)
        background(0, 0, 0)
        draw_real()
        p5.renderer.flush_geometry()
        real_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
        background(0, 0, 0)
        draw_img()
        p5.renderer.flush_geometry()
        img_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
        pixels = np.array(real_pixels, np.complex)
        dither = np.array(np.random.rand(*pixels.shape) * ditherstr.get(), np.complex)
        dither.imag = np.random.rand(*pixels.shape) * ditherstr.get()

        pixels += dither

        pixels.imag = img_pixels
        views = [np.array(np.fft.fftshift(pixels[:,:,i])) for i in range(3)]
        ffts = [
            np.fft.fftshift(touint8(np.fft.ifft2(views[i])))
            for i in range(3)
        ]
        i = np.array(np.stack(ffts,2), dtype=np.uint8)
        img = Image.fromarray(i, "RGB")
        contrast = ImageEnhance.Contrast(img.convert("RGB"))
        # cimg = contrast.enhance(1.2)

        pimg = PImage(w, h, 'RGB')
        pimg._img = contrast.enhance(10)
        pimg._load()
        p5.renderer.tint_enabled = False
        image(pimg, (0,0))
        nextframe.play()

        save_frame("{}/test.png".format(folder))
    return draw_frame



handspot_renderer = handspots_renderer(o)
circle_renderer = circles_renderer(o)
s.start()


def draw():
    # circle_renderer()
    handspot_renderer()
if __name__ == '__main__':
    run()
