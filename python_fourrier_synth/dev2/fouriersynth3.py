from p5 import *
from PIL import Image, ImageEnhance
import numpy as np
from random import randint




remmax = lambda x: x/x.max()
remmin = lambda x: x - np.amin(x, axis=(0,1), keepdims=True)
touint8 = lambda x: (remmax(remmin(x))*(256-1e-4)).astype(int)

# w = 256
# h = 256
# w = 512
# h = 512
w = 1920
h = 1080


class Pulse:
    def __init__(self, pos, speed, grow, rotate, color, max_size):
        self.pos = pos
        self.speed = speed
        self.grow = grow
        self.rotate = rotate
        self.color = color
        self.size = 2
        self.max_size = max_size
    def move(self):
        self.pos += self.speed
        self.speed.rotate(radians(self.rotate))
        self.size += self.grow
        if self.size > self.max_size or self.size <= 1:
            self.grow = - self.grow

    def draw(self):
        no_stroke()
        fill(*self.color)
        circle(self.pos, self.size)

class CircleTiles:
    def __init__(self, tilesize, grow, color, max_size):
        self.pos = Vector(0,0)
        self.grow = grow
        self.tilesize = tilesize
        self.size = tilesize
        self.max_size = max_size
        self.color = color
    def move(self):
        self.size += self.grow
        if self.size > self.max_size or self.size <= self.tilesize:
            self.grow = - self.grow
    def draw(self):
        i = 0
        j = 0
        while i < w + self.size:
            j = 0
            while j < h + self.size:
                no_stroke()
                fill(*self.color)
                circle((i,j), self.size + self.size/8)
                j+= self.size
            i+= self.size



rpulss = [Pulse(Vector(0 + randint(1,2) * i, h - randint(1,2) * 1+i), Vector(0.3/6*i,0.4/8*i), 0.4, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*20) for i in range(1,4)]
rpulss.extend([Pulse(Vector(w/2 - randint(1,2) * i, h/2 - randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.4, 1 + randint(1,2) * i/2 , (255 -i *10,55 + i * 8,55), i*20) for i in range(1,4)])
ipuls = Pulse(Vector(w/2 - 40, h/2 + 60), Vector(0.8,.06), 3, 3.2, (123,12,223), 54)
ipulss = [Pulse(Vector(w/2 + randint(1,2) * i, h/2 + randint(1,2) * 1+i), Vector(0.3/4*i,0.4/4*i), 0.2, 1 + randint(1,2) * i/3, (122,12 + i * 8,223 - i *10), i*2) for i in range(1,4)]
ipulss = []
ipulss.extend([Pulse(Vector(w/2 + randint(1,2) * i, 0 + randint(1,2) * 1+i), Vector(0.3*i,0.4*i), 0.2, 1 + randint(1,2) * i/3, (122,12 + i * 8,223 - i *10), i*2) for i in range(1,2)])
ipulss.append(CircleTiles(560, 4, (0,0,0), 670))

def draw_real():
    for rpuls in rpulss:
        rpuls.draw()
        rpuls.move()

def draw_img():
    for ipuls in ipulss:
        ipuls.draw()
        ipuls.move()

def draw():
    size(w, h)
    background(0, 0, 0)
    draw_real()
    p5.renderer.flush_geometry()
    real_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
    background(23, 23, 23)
    draw_img()
    p5.renderer.flush_geometry()
    img_pixels = p5.renderer.fbuffer.read(mode='color', alpha=False)
    pixels = np.array(real_pixels, np.complex_)
    pixels.imag = img_pixels
    views = [np.array(np.fft.fftshift(pixels[:,:,i])) for i in range(3)]
    ffts = [
        np.fft.fftshift(touint8(np.fft.ifft2(views[i])))
        for i in range(3)
    ]
    i = np.array(np.stack(ffts,2), dtype=np.uint8)
    img = Image.fromarray(i, "RGB")
    contrast = ImageEnhance.Contrast(img.convert("RGB"))
    cimg = contrast.enhance(1.2)

    pimg = PImage(w, h, 'RGB')
    pimg._img = contrast.enhance(10)
    pimg._load()
    p5.renderer.tint_enabled = False
    image(pimg, (0,0))

    # save_frame("test2/test.png")

if __name__ == '__main__':
    run()
