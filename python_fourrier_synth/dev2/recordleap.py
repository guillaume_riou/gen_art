from pyo_osc_tools import *
from pyo import *



s = Server().boot()
folder = input("recording name ? ")
# recorder = OSCRecord(folder)
# recorder.start()
# o = recorder.o
o = OSCToSig()
h1fs = o["hand"][1]["finger"]
s.start()

x = [SigTo(Scale(h1fs[i]["pos"].sig[0], -400.4, 1144.4, 0, 1), time=1/24) for i in range(5)]
y = [SigTo(Scale(h1fs[i]["pos"].sig[2], -178, 473, 0, 1), time=1/24) for i in range(5)]
z = [SigTo(Scale(h1fs[i]["pos"].sig[1], 0, 473, -1, 1), time=1/24) for i in range(5)]


snd = SndTable("./colin.wav")
env = HannTable()
pos = [xi * snd.getSize() / 5 for xi in z]
dur = [yi/20 + 1.0 for yi in x]
pitch = [zi * 0.04 for zi in y]

g = Granulator(snd, env, pitch, pos, dur, 24, mul=.1).out()

# sls = Pan([
#     SineLoop(
#         freq=MToF(),
#         mul=DBToA(Scale(h1fs[i]["pos"].sig[1], 0, 473, -76, -13)),
#         feedback=Scale(h1fs[i]["pos"].sig[2], -178, 473, 0, 0.6)
#     )
#     for i in range(5)
# ])
# sls.out()
g.out()

s.start()
s.gui(locals)
