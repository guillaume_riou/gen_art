from p5 import *
from PIL import Image, ImageEnhance
import numpy as np
from random import randint

print(PImage)


remmax = lambda x: x/x.max()
remmin = lambda x: x - np.amin(x, axis=(0,1), keepdims=True)
touint8 = lambda x: (remmax(remmin(x))*(256-1e-4)).astype(int)

h = 640
w = 360


hsv = np.array([[[0,0,0] for i in range(h)] for j in range(w)])
views = [hsv[:,:,i] for i in range(3)]
def draw():
        # The size function is a statement that tells the computer
        # how large to make the window.
        # Each function statement has zero or more parameters.
        # Parameters are data passed into the function
        # and are used as values for telling the computer what to do.
        size(640, 360)

        # The background function is a statement that tells the computer
        # which color (or gray value) to make the background of the display window

        background(204, 153, 0)
        image_mode('corner')
        hsv = np.array([[[0,0,0] for i in range(h)] for j in range(w)], np.complex_)

        views = [hsv[:,:,i] for i in range(3)]
        # print(hsv)
        x = randint(150,w-150)
        y = randint(300,h-300)
        for i in range(12):
            tx = x + randint(-50, 50)
            ty = y + randint(-50, 50)
            views[0][tx,ty] = randint(0,100)
            views[1][tx,ty] = randint(0,100)
            views[2][tx,ty] = randint(0,100)
        x = randint(150,w-150)
        y = randint(300,h-300)
        for i in range(12):
            tx = x + randint(-50, 50)
            ty = y + randint(-50, 50)
            views[0][tx,ty] = randint(0,100)
            views[1][tx,ty] = randint(0,100)
            views[2][tx,ty] = randint(0,100)
        x = randint(100,w-100)
        y = randint(100,h-100)
        for i in range(23):
            tx = x + randint(-50, 50)
            ty = y + randint(-50, 50)
            views[0][tx,ty] =  float(randint(0,100)) * 1.j
            views[1][tx,ty] =  float(randint(0,100)) * 1.j
            views[2][tx,ty] =  float(randint(0,100)) * 1.j
        # views[0][0,0] = 255
        # views[1][0,0] = 255
        # views[2][0,0] = 255
        # i = freq2im(hsv)
        print(views)
        ffts = [
            touint8(np.fft.ifft2(np.fft.fftshift(np.copy(views[i]))))
            for i in range(3)
        ]
        i = np.array(np.stack(ffts,2), dtype=np.uint8)
        # pimg = PImage()
        pimg = create_image(w,h)
        img = Image.fromarray(i, "HSV")
        print(dir(pimg))
        contrast = ImageEnhance.Contrast(img.convert("RGB"))
        p5.renderer.tint_enabled = False
        # image(contrast.enhance(2), 0, 0)
        pimg._load()
        image(pimg, (0,0))



if __name__ == '__main__':
    run()
